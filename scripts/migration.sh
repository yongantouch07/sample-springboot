#!/bin/bash
filename=$1
if [ -z "$filename" ];
 then echo "migration is required to input filename"; exit;
fi

SCRIPT=$(readlink -f "$0")
BASEDIR=$(dirname "$SCRIPT")

file="${BASEDIR}/../src/main/resources/db/migration/V$(date +"%Y%m%d%H%M%S")__${filename}.sql"
echo "" > "${file}"
echo "file has been created at $file ";