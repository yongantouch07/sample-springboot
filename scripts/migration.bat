@Echo off

:: Check WMIC is available
WMIC.EXE Alias /? >NUL 2>&1 || GOTO s_error

:: Use WMIC to retrieve date and time
FOR /F "skip=1 tokens=1-6" %%G IN ('WMIC Path Win32_LocalTime Get Day^,Hour^,Minute^,Month^,Second^,Year /Format:table') DO (
   IF "%%~L"=="" goto s_done
      SET _yyyy=%%L
      SET _mm=00%%J
      SET _dd=00%%G
      SET _hour=00%%H
      SET _minute=00%%I
      SET _second=00%%K
)
:s_done

:: Pad digits with leading zeros
      SET _mm=%_mm:~-2%
      SET _dd=%_dd:~-2%
      SET _hour=%_hour:~-2%
      SET _minute=%_minute:~-2%
      SET _second=%_second:~-2%
:: Input arg
SET filename=%1
SET file=%CD%\src\main\resources\db\migration\V%_yyyy%%_mm%%_dd%%_hour%%_minute%%_second%__%filename%.sql

:: validation
IF "%~1" == "" (
	echo migration is required to input filename
) ELSE (
	echo. 2> %file%
	echo file has been created at : %file%
)