package com.example.samplestructure.auth.service;

import com.example.samplestructure.auth.data.AuthTokenResponse;
import com.example.samplestructure.auth.data.RefreshTokenRequest;
import com.example.samplestructure.usermanagement.entity.AppUser;
import com.example.samplestructure.usermanagement.repository.AppUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@AllArgsConstructor
public class AuthService {
    private final JwtService jwtService;
    private final AppUserRepository appUserRepository;

    @Transactional(readOnly = true)
    public AuthTokenResponse generateToken(String username) {
        AppUser appUser = appUserRepository.findByUsername(username);
        String accessToken = jwtService.generateToken(appUser);
        String refreshToken = jwtService.generateRefreshToken(appUser);

        return new AuthTokenResponse()
                .setAccessToken(accessToken)
                .setRefreshToken(refreshToken)
                .setUserId(appUser.getId())
                .setUsername(appUser.getUsername());
    }

    @Transactional(readOnly = true)
    public AuthTokenResponse generateTokenFromRefreshToken(RefreshTokenRequest request) {
        String refreshToken = request.getRefreshToken();
        String username = jwtService.extractUsername(refreshToken);
        AppUser appUser = Optional.ofNullable(appUserRepository.findByUsername(username))
                .orElseThrow(() -> new UsernameNotFoundException("Username not found!"));

        jwtService.validateToken(refreshToken, appUser);

        String accessToken = jwtService.generateToken(appUser);
        String newRefreshToken = jwtService.generateRefreshToken(appUser);
        return new AuthTokenResponse()
                .setAccessToken(accessToken)
                .setRefreshToken(newRefreshToken)
                .setUserId(appUser.getId())
                .setUsername(appUser.getUsername());
    }
}
