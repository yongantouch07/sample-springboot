package com.example.samplestructure.auth.service;

import com.example.samplestructure.usermanagement.entity.AppUser;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtService {
    private static final String SECRET = "#@#5367566B59703373367639792F423F4528482B4D6251655468576D5A71347437";
    private static final Long EXPIRATION_TOKEN = 1000 * 60 * 30L; // Expire in 30 minutes
    private static final Long EXPIRATION_REFRESH_TOKEN = 1000 * 60 * 60L;

    private Key getSignKey() {
        byte[] bytes = Decoders.BASE64.decode(SECRET);
        return Keys.hmacShaKeyFor(bytes);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parser()
                .verifyWith((SecretKey) getSignKey())
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public void validateToken(String token, AppUser appUser) {
        final String username = extractUsername(token);
        if (isTokenExpired(token)) {
            throw new ExpiredJwtException(null, null, "JWT token was expired");
        }
        if (!username.equalsIgnoreCase(appUser.getUsername())) {
            throw new JwtException("Invalid JWT Token");
        }
    }

    public String generateToken(AppUser appUser) {
        // Claims is content we want to store in jwt token
        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", appUser.getId());
        claims.put("email", appUser.getEmail());
        return createToken(claims, appUser.getUsername(), EXPIRATION_TOKEN);
    }

    public String generateRefreshToken(AppUser appUser) {
        // Claims is content we want to store in jwt token
        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", appUser.getId());
        claims.put("email", appUser.getEmail());
        return createToken(claims, appUser.getUsername(), EXPIRATION_REFRESH_TOKEN);
    }

    private String createToken(Map<String, Object> claims,
                               String userName,
                               Long expiration) {
        return Jwts.builder()
                .claims(claims)
                .subject(userName)
                .issuedAt(new Date(System.currentTimeMillis()))
                .expiration(new Date(System.currentTimeMillis() + expiration))
                .signWith(getSignKey())
                .compact();
    }
}
