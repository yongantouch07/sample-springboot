package com.example.samplestructure.auth.data;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class AuthTokenResponse {
    private String accessToken;
    private String refreshToken;
    private Long userId;
    private String username;
}
