package com.example.samplestructure.auth.controller;

import com.example.samplestructure.auth.data.AuthRequest;
import com.example.samplestructure.auth.data.AuthTokenResponse;
import com.example.samplestructure.auth.data.RefreshTokenRequest;
import com.example.samplestructure.auth.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final AuthService authService;

    @PostMapping("/token")
    public ResponseEntity<AuthTokenResponse> getToken(@RequestBody @Validated AuthRequest request) {
        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        Authentication authenticated = authenticationManager.authenticate(token);
        AuthTokenResponse authTokenResponse = authService.generateToken(authenticated.getName());
        return ResponseEntity.ok(authTokenResponse);
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<AuthTokenResponse> getRefreshToken(@RequestBody @Validated RefreshTokenRequest request) {
        AuthTokenResponse authTokenResponse = authService.generateTokenFromRefreshToken(request);
        return ResponseEntity.ok(authTokenResponse);
    }
}
