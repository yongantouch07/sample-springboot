package com.example.samplestructure.infrastructure.customfilter;

import com.example.samplestructure.auth.service.JwtService;
import com.example.samplestructure.usermanagement.entity.AppUser;
import com.example.samplestructure.usermanagement.service.CustomUserDetailService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import java.io.IOException;

@Component
@AllArgsConstructor
public class JwtRequestFilter extends OncePerRequestFilter {
    private static final String AUTH_HEADER = "Authorization";
    private final HandlerExceptionResolver handlerExceptionResolver;
    private final JwtService jwtService;
    private final CustomUserDetailService customUserDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        try {
            String authHeader = request.getHeader(AUTH_HEADER);
            String token = null;
            if (StringUtils.hasText(authHeader) && authHeader.startsWith("Bearer")) {
                token = authHeader.substring(7);
                String username = jwtService.extractUsername(token);
                AppUser appUser = customUserDetailService.findUserWithRoleAndPermission(username);
                jwtService.validateToken(token, appUser);
                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(appUser.getUsername(), null, appUser.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
            filterChain.doFilter(request, response);
        } catch (MalformedJwtException | ExpiredJwtException | BadCredentialsException ex) {
            handlerExceptionResolver.resolveException(request, response, null, ex);
        }
    }
}
