package com.example.samplestructure.infrastructure.exception.controller;

import com.example.samplestructure.infrastructure.exception.custom.ApiErrorValidationException;
import com.example.samplestructure.infrastructure.exception.data.ApiParameterError;
import com.example.samplestructure.infrastructure.exception.data.GlobalErrorResponse;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.*;

@ControllerAdvice
public class ExceptionAdviceController {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<GlobalErrorResponse> handleValidateException(MethodArgumentNotValidException ex) {
        List<ApiParameterError> errors = ex.getBindingResult()
                .getAllErrors()
                .stream()
                .map(e -> {
                    DefaultMessageSourceResolvable resolvable =
                            (DefaultMessageSourceResolvable) Objects.requireNonNull(e.getArguments())[0];
                    String parameter = resolvable.getCode();
                    return new ApiParameterError()
                            .setParameter(parameter)
                            .setMessage(e.getDefaultMessage());
                })
                .toList();
        GlobalErrorResponse errorResponse = new GlobalErrorResponse()
                .setErrorCode("400")
                .setMessage("Validation error exists")
                .setDetail(ex.getMessage())
                .setErrors(errors);

        return ResponseEntity.badRequest()
                .body(errorResponse);
    }

    @ExceptionHandler(value = ApiErrorValidationException.class)
    public ResponseEntity<GlobalErrorResponse> handleAccessDeniedException(ApiErrorValidationException ex) {
        GlobalErrorResponse errorResponse = new GlobalErrorResponse()
                .setErrorCode("400")
                .setMessage("Validation error exists")
                .setErrors(ex.getErrors());

        return ResponseEntity.badRequest()
                .body(errorResponse);
    }

    @ExceptionHandler(value = MalformedJwtException.class)
    public ResponseEntity<GlobalErrorResponse> handleMalformedJwtException(MalformedJwtException ex) {
        GlobalErrorResponse errorResponse = new GlobalErrorResponse()
                .setErrorCode("401")
                .setMessage("Invalid JWT Token")
                .setDetail(ex.getMessage());

        return ResponseEntity.status(401)
                .body(errorResponse);
    }

    @ExceptionHandler(value = ExpiredJwtException.class)
    public ResponseEntity<GlobalErrorResponse> handleExpiredJwtException(ExpiredJwtException ex) {
        GlobalErrorResponse errorResponse = new GlobalErrorResponse()
                .setErrorCode("401")
                .setMessage("JWT Token was expired")
                .setDetail(ex.getMessage());

        return ResponseEntity.status(401)
                .body(errorResponse);
    }

    @ExceptionHandler(value = BadCredentialsException.class)
    public ResponseEntity<GlobalErrorResponse> handleAuthenticationException(BadCredentialsException ex) {
        GlobalErrorResponse errorResponse = new GlobalErrorResponse()
                .setErrorCode("401")
                .setMessage("Username or password is invalid")
                .setDetail(ex.getMessage());

        return ResponseEntity.status(401)
                .body(errorResponse);
    }

    @ExceptionHandler(value = InsufficientAuthenticationException.class)
    public ResponseEntity<GlobalErrorResponse> handleInsufficientAuthenticationException(InsufficientAuthenticationException ex) {
        GlobalErrorResponse errorResponse = new GlobalErrorResponse()
                .setErrorCode("401")
                .setMessage("Unauthenticated user required to login")
                .setDetail(ex.getMessage());

        return ResponseEntity.status(401)
                .body(errorResponse);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<GlobalErrorResponse> handleAccessDeniedException(AccessDeniedException ex) {
        GlobalErrorResponse errorResponse = new GlobalErrorResponse()
                .setErrorCode("403")
                .setMessage("insufficient privileges to complete the operation")
                .setDetail(ex.getMessage());

        return ResponseEntity.status(403)
                .body(errorResponse);
    }

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<GlobalErrorResponse> handleRuntimeException(RuntimeException ex) {
        GlobalErrorResponse errorResponse = new GlobalErrorResponse()
                .setErrorCode("500")
                .setMessage("Unknown internal server error")
                .setDetail(ex.getMessage());

        return ResponseEntity.status(500)
                .body(errorResponse);
    }
}
