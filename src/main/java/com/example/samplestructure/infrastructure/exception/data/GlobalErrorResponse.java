package com.example.samplestructure.infrastructure.exception.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Setter
@Getter
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GlobalErrorResponse {
    private String errorCode;
    private String message;
    private String detail;
    private List<ApiParameterError> errors;
}
