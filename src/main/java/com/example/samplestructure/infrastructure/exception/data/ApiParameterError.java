package com.example.samplestructure.infrastructure.exception.data;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ApiParameterError {
    private String parameter;
    private String message;
}
