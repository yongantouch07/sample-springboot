package com.example.samplestructure.infrastructure.exception.custom;

import com.example.samplestructure.infrastructure.exception.data.ApiParameterError;
import lombok.Getter;

import java.util.Collections;
import java.util.List;

@Getter
public class ApiErrorValidationException extends RuntimeException {
    private final List<ApiParameterError> errors;

    public ApiErrorValidationException(List<ApiParameterError> errors) {
        this.errors = errors;
    }

    public ApiErrorValidationException(ApiParameterError error) {
        this.errors = Collections.singletonList(error);
    }
}
