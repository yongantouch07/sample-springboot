package com.example.samplestructure.customize.basedentity;

import jakarta.persistence.*;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.AbstractPersistable;

import java.util.Objects;

@MappedSuperclass
public class CustomAbstractPersistable implements Persistable<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public boolean isNew() {
        return Objects.isNull(id);
    }
}
