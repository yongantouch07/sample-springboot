package com.example.samplestructure.usermanagement.mapper;

import com.example.samplestructure.usermanagement.data.UserData;
import com.example.samplestructure.usermanagement.data.UserRequest;
import com.example.samplestructure.usermanagement.entity.AppUser;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AppUserMapper {
    AppUser mapUserRequestToEntity(UserRequest request);

    UserData mapEntityToUserData(AppUser appUser);
}
