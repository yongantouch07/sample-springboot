package com.example.samplestructure.usermanagement.entity;

import com.example.samplestructure.customize.basedentity.CustomAbstractPersistable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "permission")
public class Permission extends CustomAbstractPersistable {

    @Column(name = "description")
    private String description;

    @Column(name = "code")
    private String code;

    @Column(name = "action")
    private String action;

    @Column(name = "entity")
    private String entity;

    @Column(name = "grouping")
    private String grouping;

}
