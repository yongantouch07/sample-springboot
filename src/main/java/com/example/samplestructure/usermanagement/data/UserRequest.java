package com.example.samplestructure.usermanagement.data;

import jakarta.validation.constraints.*;
import lombok.Data;

import java.util.List;

@Data
public class UserRequest {
    @NotBlank
    private String firstname;

    @NotBlank
    private String lastname;

    @NotBlank
    private String username;

    @Email
    @NotBlank
    private String email;

    private String phone;

    @NotBlank
    @Min(8)
    private String password;

    @NotBlank
    @Min(8)
    private String confirmPassword;

    @NotNull
    @Size(min = 1)
    private List<Long> roleIds;
}
