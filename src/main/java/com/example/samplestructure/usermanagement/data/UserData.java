package com.example.samplestructure.usermanagement.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserData {
    private Long id;
    private String firstname;
    private String lastname;
    private String username;
    private String email;
    private String phone;
}
