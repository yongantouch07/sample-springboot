package com.example.samplestructure.usermanagement.controller;

import com.example.samplestructure.usermanagement.data.UserData;
import com.example.samplestructure.usermanagement.data.UserRequest;
import com.example.samplestructure.usermanagement.service.AppUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/users")
@AllArgsConstructor
public class UserController {
    private final AppUserService appUserService;

    @GetMapping("{userId}")
    @PreAuthorize("hasAnyAuthority('special.all', 'users.read')")
    public ResponseEntity<UserData> getUserById(@PathVariable("userId") Long userId) {
        UserData userData = appUserService.getUserById(userId);
        return ResponseEntity.ok(userData);
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('special.all', 'users.create')")
    public ResponseEntity<Object> createUser(@RequestBody @Validated UserRequest request) {
        Long userId = appUserService.create(request);
        Map<String, Object> response = new HashMap<>();
        response.put("userId", userId);
        return ResponseEntity.ok(response);
    }
}
