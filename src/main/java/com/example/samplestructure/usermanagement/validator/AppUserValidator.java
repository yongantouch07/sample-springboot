package com.example.samplestructure.usermanagement.validator;

import com.example.samplestructure.infrastructure.exception.custom.ApiErrorValidationException;
import com.example.samplestructure.infrastructure.exception.data.ApiParameterError;
import com.example.samplestructure.usermanagement.data.UserRequest;
import com.example.samplestructure.usermanagement.entity.Role;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppUserValidator {
    public void validateOnCreate(UserRequest request) {
        if (!request.getPassword().equals(request.getConfirmPassword())) {
            ApiParameterError error = new ApiParameterError()
                    .setParameter("confirmPassword")
                    .setMessage("Password not match!");
            throw new ApiErrorValidationException(error);
        }
    }

    public void validateRoleId(UserRequest request, List<Role> roles) {
        if (roles.isEmpty()) {
            String ids = String.join(",", request.getRoleIds().stream().map(String::valueOf).toList());
            String msg = "RoleId = [%s] not exists in system".formatted(ids);
            ApiParameterError error = new ApiParameterError()
                    .setParameter("roleIds")
                    .setMessage(msg);
            throw new ApiErrorValidationException(error);
        }

        List<Long> roleIds = roles.stream().map(Role::getId).toList();
        List<String> missingRoleIds = new ArrayList<>();
        for (Long id : request.getRoleIds()) {
            if (!roleIds.contains(id)) {
                missingRoleIds.add(String.valueOf(id));
            }
        }

        if (!missingRoleIds.isEmpty()) {
            String ids = String.join(",", missingRoleIds);
            String msg = "RoleId = [%s] not exists in system".formatted(ids);
            ApiParameterError error = new ApiParameterError()
                    .setParameter("roleIds")
                    .setMessage(msg);
            throw new ApiErrorValidationException(error);
        }
    }
}
