package com.example.samplestructure.usermanagement.repository;

import com.example.samplestructure.usermanagement.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
