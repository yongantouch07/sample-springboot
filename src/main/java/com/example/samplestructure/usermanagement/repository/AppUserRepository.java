package com.example.samplestructure.usermanagement.repository;

import com.example.samplestructure.usermanagement.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    @Query("SELECT u FROM AppUser u WHERE u.username = :username")
    AppUser findByUsername(@Param("username") String username);

    @Query("SELECT u FROM AppUser u LEFT JOIN FETCH u.roles r WHERE u.username = :username")
    AppUser findByUsernameWithFetchRoles(@Param("username") String username);
}
