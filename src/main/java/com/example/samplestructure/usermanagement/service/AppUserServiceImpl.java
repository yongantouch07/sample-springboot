package com.example.samplestructure.usermanagement.service;

import com.example.samplestructure.infrastructure.exception.custom.ResourceNotFoundException;
import com.example.samplestructure.usermanagement.data.UserData;
import com.example.samplestructure.usermanagement.data.UserRequest;
import com.example.samplestructure.usermanagement.entity.AppUser;
import com.example.samplestructure.usermanagement.entity.Role;
import com.example.samplestructure.usermanagement.mapper.AppUserMapper;
import com.example.samplestructure.usermanagement.repository.AppUserRepository;
import com.example.samplestructure.usermanagement.repository.RoleRepository;
import com.example.samplestructure.usermanagement.validator.AppUserValidator;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AppUserServiceImpl implements AppUserService {
    private final AppUserRepository appUserRepository;
    private final AppUserMapper appUserMapper;
    private final AppUserValidator appUserValidator;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public Long create(UserRequest request) {
        appUserValidator.validateOnCreate(request);
        List<Role> roles = roleRepository.findAllById(request.getRoleIds());
        appUserValidator.validateRoleId(request, roles);
        AppUser appUser = appUserMapper.mapUserRequestToEntity(request);
        appUser.setPassword(passwordEncoder.encode(request.getPassword()));
        appUser.setRoles(roles);
        appUserRepository.save(appUser);
        return appUser.getId();
    }

    @Override
    public UserData getUserById(Long userId) {
        AppUser appUser = appUserRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("UserId = %s not found!".formatted(userId)));
        return appUserMapper.mapEntityToUserData(appUser);
    }

}
