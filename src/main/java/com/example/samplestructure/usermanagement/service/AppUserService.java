package com.example.samplestructure.usermanagement.service;

import com.example.samplestructure.usermanagement.data.UserData;
import com.example.samplestructure.usermanagement.data.UserRequest;

public interface AppUserService {
    Long create(UserRequest request);

    UserData getUserById(Long userId);
}
