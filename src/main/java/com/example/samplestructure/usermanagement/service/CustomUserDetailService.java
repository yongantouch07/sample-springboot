package com.example.samplestructure.usermanagement.service;

import com.example.samplestructure.usermanagement.entity.AppUser;
import com.example.samplestructure.usermanagement.repository.AppUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final AppUserRepository appUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = appUserRepository.findByUsername(username);
        return Optional.ofNullable(appUser)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found!"));
    }

    @Transactional(readOnly = true)
    public AppUser findUserWithRoleAndPermission(String username) {
        AppUser appUser = Optional.ofNullable(appUserRepository.findByUsername(username))
                .orElseThrow(() -> new UsernameNotFoundException("Username not found!"));
        appUser.getRoles().stream().flatMap(r -> r.getPermissions().stream()).toList();
        return appUser;
    }
}
